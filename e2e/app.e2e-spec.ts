import { AppcountriesPage } from './app.po';

describe('appcountries App', function() {
  let page: AppcountriesPage;

  beforeEach(() => {
    page = new AppcountriesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
