import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {CountriesComponent} from "./components/countries/countries.component";
import {NavbarComponent} from "./components/elements/navbar/navbar.component";
import {FooterComponent} from "./components/elements/footer/footer.component";
import {CountriesService} from "./services/countries.service";
import {Messages} from "./components/elements/messages/menssages";
import {MapsComponent} from "./components/maps/maps.component";

@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    NavbarComponent,
    FooterComponent,
    MapsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    CountriesService,
    Messages
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
