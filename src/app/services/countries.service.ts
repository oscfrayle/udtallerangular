import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";

@Injectable()
export class CountriesService {

  private url:string = "https://restcountries.eu/rest/v2/";

  constructor(private http: Http) { }

  public getCountries() : Observable<Response> {
    // console.log(this.http.get(this.url));
     return this.http.get(this.url + 'all')
                .catch(()=>Observable.throw('error..'));
    // console.log('estomos en el servicio');
  }

  public getFilter(key) : Observable<Response> {
    // console.log(this.http.get(this.url));
    return this.http.get(this.url + `name/${key}`)
      .catch(()=>Observable.throw('error..'));
    // console.log('estomos en el servicio');
  }
}
