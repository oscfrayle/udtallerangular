import { Component, OnInit } from '@angular/core';
import {CountriesService} from "../../services/countries.service";
import {Messages} from "../elements/messages/menssages";
declare var google:any;

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  public countries:Array<Object>;

  constructor(private oServicesCountries: CountriesService, private oMessages: Messages) { }

  ngOnInit() {
    this.getCountries()
  }

  public getCountries(){

      let self = this;
      this.oServicesCountries.getCountries()
        .subscribe(
          // countries => this.countries = countries.json(),
          function(response){
            self.oMessages.show_message(`STATUS: ${response.status }`);
            // console.log(response);
            // console.log(response.json());
            if(response.status == 200){
              self.countries = response.json();
              // console.log(response.json());
            }

          },
          error => console.log('error'),
          // () => console.log(this.countries)
          () => self.oMessages.show_message('Servicio consumido')
        );
    console.log(this.oServicesCountries.getCountries());
  }

  public getFilter(key){

    let self = this;
    this.oServicesCountries.getFilter(key)
      .subscribe(
        // countries => this.countries = countries.json(),
        function(response){
          // console.log(response);
          // console.log(response.json());
          self.oMessages.show_message(`STATUS: ${response.status }`);
          if(response.status == 200){

            self.countries = response.json();
            // console.log(response.json());
          }

        },
        error => console.log('error'),
        // () => console.log(this.countries)
        ()=> self.oMessages.show_message('Servicio consumido')
      );
    console.log(this.oServicesCountries.getCountries());
  }

  public getMap(latlog){
    var map
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latlog[0], lng: latlog[1]},
      zoom: 8
    });

    this.oMessages.show_message(`MAP: ${latlog[0]} / ${latlog[1]} `)
  }

}
